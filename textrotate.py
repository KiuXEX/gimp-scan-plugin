#!/usr/bin/env python

from gimpfu import *
def python_fu_textrotate(img, drw):
	if ( not pdb.gimp_item_is_text_layer(drw)):
		return -1

	img.undo_group_start()

	vec = pdb.gimp_vectors_new_from_text_layer(img, drw)
	pdb.gimp_image_add_vectors(img, vec, 0)
	txt = drw.copy()
	txt.name = "Text"
	bgl = gimp.Layer(img, "Fill", img.width, img.height, drw.type, 100, drw.mode)
	grp = gimp.GroupLayer(img, drw.name)
	pos = pdb.gimp_image_get_item_position(img, drw)
	par = pdb.gimp_item_get_parent(drw)
	img.remove_layer(drw)

	img.insert_layer(grp, par, pos)
	img.insert_layer(bgl, grp)
	img.insert_layer(txt, grp)

	pdb.gimp_item_set_visible(txt, FALSE)
	pdb.gimp_item_set_visible(vec, TRUE)
	img.active_layer = bgl

	img.undo_group_end()

register(
	"python_fu_textrotate",
	"Create vector for text layer.",
	"Create vector for text layer. Layer will be moved inside of a group with the same name, then renamed to 'Text'. Transparent layer will be created, and named 'Fill'. This transparent layer will be used to fill in the rotated text and must be cropped to content.",
	"KiuXEX",
	"CC0",
	"2019",
	"<Image>/Python-Fu/Manga/Text to vector",
	"RGB*, GRAY*",
	[],
	[],
	python_fu_textrotate)

main()
