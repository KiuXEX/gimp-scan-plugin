#!/usr/bin/env python

from gimpfu import *
def python_fu_manga_mkpage(img, drw):
	img.undo_group_start()
	orig = drw.copy()
	orig.name = "Original"
	grp = gimp.GroupLayer(img, "Page")
	img.add_layer(grp, 0)
	img.remove_layer(drw)
	img.insert_layer(orig, grp)
	mask = gimp.Layer(img, "Mask", img.width, img.height, orig.type, 100, orig.mode)
	img.insert_layer(mask, grp)
	img.active_layer = mask;
	img.undo_group_end()

register(
	"python_fu_manga_mkpage",
	"Setup page group.",
	"Creates layer group called 'Page' and moves active layer into it. Creates a new layer called 'Mask' inside the Page group. Select the Mask layer.",
	"KiuXEX",
	"CC0",
	"2019",
	"<Image>/Python-Fu/Manga/Make Page",
	"RGB*, GRAY*",
	[],
	[],
	python_fu_manga_mkpage)

main()
