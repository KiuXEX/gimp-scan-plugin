#!/usr/bin/env python

from gimpfu import *
def python_fu_vectorbg(img, drw, i):
	vec = pdb.gimp_image_get_active_vectors(img)
	if (vec == -1):
		return

	img.undo_group_start()

	sel = None
	if not (pdb.gimp_selection_is_empty(img)):
		sel = img.selection.copy()

	pdb.gimp_image_select_item(img, CHANNEL_OP_REPLACE, vec)
	if (i > 0):
		pdb.gimp_selection_grow(img, i)
		pdb.gimp_edit_fill(drw, FILL_BACKGROUND)
		pdb.gimp_image_select_item(img, CHANNEL_OP_REPLACE, vec)

	pdb.gimp_edit_fill(drw, FILL_FOREGROUND)
	pdb.plug_in_autocrop_layer(img, drw)
	pdb.gimp_item_set_visible(vec, FALSE)

	if (sel):
		img.add_channel(sel)
		pdb.gimp_image_select_item(img, CHANNEL_OP_REPLACE, sel)
		img.remove_channel(sel)
	else:
		pdb.gimp_selection_none(img)

	img.undo_group_end()

register(
	"python_fu_vectorbg",
	"Border and fill vector.",
	"Create border and fill for vector on selected layer. Prompts for border size. Fill is FG color, border is BG color. If border is smaller than 1, skip border fill.",
	"KiuXEX",
	"CC0",
	"2019",
	"<Image>/Python-Fu/Manga/VectorBG",
	"RGB*, GRAY*",
	[
		( PF_INT, "Border", "Border size", 2 )
	],
	[],
	python_fu_vectorbg)

main()
