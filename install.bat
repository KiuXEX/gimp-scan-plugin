@echo off
set installdir=%appdata%\GIMP\2.10\plug-ins
echo Scripts will be installed to:
echo %installdir%
choice /C YNC /M "Is this correct? Press C to input custom install path. Press N to abort."

if %errorlevel% == 2 exit
if %errorlevel% == 3 set /p installdir="Input path:"

copy *.py %installdir%
