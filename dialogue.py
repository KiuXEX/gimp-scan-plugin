#!/usr/bin/env python

from gimpfu import *
def python_fu_manga_dialogue(img, drw, n):

	img.undo_group_start()

	dial = gimp.GroupLayer(img, "Dialogue")
	img.add_layer(dial, 0)

	for i in range(1,n+1):
		group = gimp.GroupLayer(img, "Panel %i" % i)
		img.insert_layer(group, dial, i)

	img.undo_group_end()
		

register(
	"python_fu_manga_dialogue",
	"Create layer groups for panels.",
	"Create layer groups for neatly organising manga dialogue. Prompts for number of panels.",
	"KiuXEX",
	"CC0",
	"2019",
	"<Image>/Python-Fu/Manga/Dialogue",
	"RGB*, GRAY*",
	[
		( PF_INT, "Panels", "Number of panels", 8 )
	],
	[],
	python_fu_manga_dialogue)

main()
