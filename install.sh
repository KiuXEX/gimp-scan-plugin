#!/bin/sh
# Installation script.
gimp_path="$(find ~/.config -type d -name plug-ins | grep GIMP | sed '1q' )"
if [ -z "$gimp_path" ]; then
	printf 'No GIMP installation detected. Input custom directory? [y/n] '
	read bool
	[ "$bool" = "y" ] && printf 'Input GIMP plug-in folder: ' || exit 1
else
	printf 'GIMP plug-in directory detected as:\n\e[32m%s\e[0m\n\nIs this correct? [y/n] ' "$gimp_path"
	read bool
fi

if [ "$bool" = "y" ]; then
	for file in *.py; do
		cp "$file" "$gimp_path" && chmod 755 "$gimp_path"/"$file"
	done
	echo 'Installation complete.'
else
	echo 'Installation failed.'
fi
