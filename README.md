A collection of scripts for easier scanlation of manga. Descriptions of the scripts:

- **dialogue.py**:	Create layer groups for neatly organising manga dialogue. Prompts for number of panels.
- **mkpage.py**:	Creates layer group called 'Page' and moves active layer into it. Creates a new layer called 'Mask' inside the Page group. Select the Mask layer.
- **textbg.py**:	Create border for text layer. Color will be BG color. Layer is converted into a group of the same name, containing a text layer called 'Text' and a raster layer called 'BG', which will contain the border.
- **textrotate.py**:	Create vector for text layer. Layer will be moved inside of a group with the same name, then renamed to 'Text'. Transparent layer will be created, and named 'Fill'. This transparent layer will be used to fill in the rotated text and must be cropped to content.
- **vectorfill.py**:	Create border and fill for vector on selected layer. Prompts for border size. Fill is FG color, border is BG color. If border is smaller than 1, skip border fill.
