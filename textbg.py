#!/usr/bin/env python

from gimpfu import *
def python_fu_textbg(img, drw, i):
	if ( not pdb.gimp_item_is_text_layer(drw)):
		return -1

	img.undo_group_start()

	sel = None
	if not (pdb.gimp_selection_is_empty(img)):
		sel = img.selection.copy()

	vec = pdb.gimp_vectors_new_from_text_layer(img, drw)
	pdb.gimp_image_add_vectors(img, vec, 0)
	pdb.gimp_image_select_item(img, CHANNEL_OP_REPLACE, vec)
	pdb.gimp_selection_grow(img, i)
	txt = drw.copy()
	txt.name = "Text"
	bgl = gimp.Layer(img, "BG", img.width, img.height, drw.type, 100, drw.mode)
	grp = gimp.GroupLayer(img, drw.name)
	pos = pdb.gimp_image_get_item_position(img, drw)
	par = pdb.gimp_item_get_parent(drw)
	img.remove_layer(drw)

	img.insert_layer(grp, par, pos)
	img.insert_layer(bgl, grp)
	img.insert_layer(txt, grp)

	img.active_layer = bgl
	pdb.gimp_edit_fill(bgl, FILL_BACKGROUND)
	pdb.plug_in_autocrop_layer(img, bgl)
	pdb.gimp_image_remove_vectors(img, vec)

	if (sel):
		img.add_channel(sel)
		pdb.gimp_image_select_item(img, CHANNEL_OP_REPLACE, sel)
		img.remove_channel(sel)
	else:
		pdb.gimp_selection_none(img)

	img.active_layer = grp
	pdb.gimp_item_set_expanded(grp, FALSE)

	img.undo_group_end()

register(
	"python_fu_textbg",
	"Create border for text layer.",
	"Create border for text layer. Color will be BG color. Layer is converted into a group of the same name, containing a text layer called 'Text' and a raster layer called 'BG', which will contain the border.",
	"KiuXEX",
	"CC0",
	"2019",
	"<Image>/Python-Fu/Manga/TextBG",
	"RGB*, GRAY*",
	[
		( PF_INT, "Border", "Border size", 2 )
	],
	[],
	python_fu_textbg)

main()
